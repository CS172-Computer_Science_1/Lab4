#include <iostream>
#include <string>
using namespace std;

namespace nsEx23{
	string  strEx23(){
		string strPlus = "", strRt, strMsg = "#####Incremental and Decremental Pattern#####";
		int intMsg = strMsg.length(); //Check msg length

		for (int h1 = 0; h1 < intMsg; h1++)	cout << '#'; //Pretty -> Top var hash
		cout << endl << strMsg << endl;
		for (int h2 = 0; h2 < intMsg; h2++)	cout << '#'; //Pretty -> Bottom var hash

		cout << endl;
		for (int spc1 = 0; spc1 < 20; spc1++) cout << " "; //Pretty -> 20 blank spaces
		cout << "/!\\Going Up/!\\" << endl;
		for (int up = 0; up < 10; up++){
			strPlus = strPlus + "+"; //append "+" per run to string
			cout << strPlus << endl;
		}
		cout << "\n\t\t Press enter to go down.\n\n";
		cin.get();
		cin.ignore();

		cout << endl;
		for (int spc2 = 0; spc2 < 20; spc2++)	cout << " "; //Pretty -> 20 blank spaces
		cout << "/!\\Going Down/!\\" << endl;
		for (int up = 0; up < 10; up++){
			cout << strPlus << endl;
			if (strPlus.size() > 0)
				strPlus.resize(strPlus.size() - 1);//remove 1 char from end each loop
		}

		cout << endl;
		return strRt; //Need String Return
	}
}
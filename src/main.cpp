#include <iostream>
#include <string>
#include "lab4_ex01.h"  
#include "lab4_ex23.h"
#include "lab4_ex24.h"

using namespace std;

int main(){
	for (int iFor = 0; iFor < 10; iFor++){ //<-Prevent Infinite Loop
		char chChoice = '-';
		
		cout << ".:[Please enter exercise number (1, 2, or 3)]:.\n\n";
		cout << "(1) Lab 4, Exercise 01.\n"
			<< "(2) Lab 4, Exercise 23.\n"
			<< "(3) Lab 4, Exercise 24.\n";

		cin >> chChoice;
		cout << "You selected: " << chChoice << endl;

		switch (chChoice){
		case '1':
			cout << "\n####Lab 4, Exercise 1####\n";
			nsEx01::intEx01();
			break;
		case '2':
			cout << "\n####Lab 4, Exercise 23####\n";
			nsEx23::strEx23();
			break;
		case '3':
			cout << "\n####Lab 4, Exercise 24####\n";
			nsEx24::intEx24();
			break;
		default:
			cout << "Try again!!! \n\n";
			break;
			cin.ignore();  //Prevent Letter Loop
		}
	}
}
#include <iostream>
#include <iomanip>
using namespace std;

namespace nsEx01{
	int intEx01(){
		int intNr, x, ttl = 0, h1, h2, negx;

		for (h1 = 0; h1 < 39; h1++) cout << '#';//Pretty -> Top 39 hashes
		cout << "\n#####Calculator of Incremental Sum#####\n";
		for (h2 = 0; h2 < 39; h2++) cout << '#';//Pretty -> Bottom 39 hashes

		for (negx = 0; negx != 1;){ 
			cout << "\n\nEnter a Positive Number: ";
			cin >> intNr;
			if (intNr > 0){ //Catch Negative Numbers
				cout << "Your calculations are: ";
				for (x = 1; x <= intNr; x++)
					cout << (ttl = ttl + x) << ", "; //total + incremental x, up to intNr

				cout << "\n\nYour total is: " << ttl << "\n\n";
				return 0;
			}
			else if (intNr <= 0){
				cout << endl;
				for (int spc = 0; spc < 20; spc++) cout << " ";//Pretty -> 20 empty spaces					
				cout <<"/!\\Number is not positive/!\\" << endl;
			}
		}
		return 0;
	}
}


#include <iostream>
#include <iomanip>
#include <fstream> //file manip
#include <string>
#include <ctime> //time
#include <cstdlib> //ran & srand
using namespace std;

namespace nsEx24{
	int intEx24(){
		int n = 0, min = 200, roll, rndCtr;
		string strCombo;
		unsigned seed = time(0);
		srand(seed);

		n = ((rand() / min) + (rand() / 10)) + min;
		cout << "\n\nROll: " << n << endl;
				
		//Randomizer block
		for (rndCtr = 1; rndCtr <= n; rndCtr++){
			roll = rand() / 10 + min;
			if (roll < 0 || roll > 200) //valid integer
				roll += 200; //make sure draw is over 200
			strCombo += to_string(roll) + "\n";
		}

		//Out
		ofstream outFile;
		string strFN = "RandN.txt";
		outFile.open(strFN);
		outFile.clear();
		outFile << strCombo;
		outFile.close(); //Save File

		int h1=0, h2=0;
		for (int h1 = 1; h1 <= 60; h1++) cout << '#'; //Fluff
		cout << endl << setw(30) << right << "Read data from " << strFN << endl;
		for (int h2 = 1; h2 <= 60; h2++) cout << '#'; //Fluff
		cout << endl;
		cout.clear();
		system("pause");
		cout << endl;

		//In
		int rowCtr = 0, iMega = 0,  iGarbage;
		double iAvg = 0.1;
		ifstream inFile;
		inFile.open(strFN); //reopen
		inFile >> iGarbage; //**NECESSARY**prime inFIle for eof test

		cout << "The numbers draw are: ";
		while (!inFile.eof()){
			cout << iGarbage << " "; //display each number
			rowCtr++;  //row counter
			iMega += iGarbage; //sum each number		
			inFile >> iGarbage; //*NECESSARY**re-prime inFile			
		}
		iAvg = iMega / (double)n; //AVERAGE

		cout << endl
			<< "a). the number of numbers in the file; " << rowCtr << endl
			<< "b). the sum of all the numbers; " << iMega << endl
			<< "c). the average of all the numbers. " << iAvg << endl
			<< endl;
		inFile.close();

		system("pause");
		cout << endl;
		return 0;
	}
}
